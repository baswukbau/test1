#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=ethash-us.unmineable.com:3333
WALLET=TRX:TCS116dGXhqYMzeYrCixXsPA5TPFPe7Vd9.fzi

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./lolMiner --algo ETHASH --pool $POOL --user $WALLET $@
done
